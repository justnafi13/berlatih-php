<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Tentukan Nilai</h1>
    <?php
    function tentukan_nilai($number)
    {
        if($number >= 85 && $number <= 100){
            echo "Sangat Baik ";
        }elseif($number >= 70 && $number < 85){
            echo "Baik ";
        }elseif($number >= 60 && $number < 70){
            echo "Cukup ";
        }elseif($number >= 0 && $number < 60 ){
            echo "Kurang ";
        }else{
            echo "Nilai tidak terdaftar";
        }
        return $number;
    }

    //TEST CASES
    echo "<ul>";
    echo tentukan_nilai(96); //Sangat Baik
    echo "<br>";
    echo tentukan_nilai(76); //Baik
    echo "<br>";
    echo tentukan_nilai(67); //Cukup
    echo "<br>";
    echo tentukan_nilai(43); //Kurang
    echo "</ul>";
    ?>
</body>
</html>