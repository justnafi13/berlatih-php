<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Tentukan Besar Kecil</h1>
    <?php
    
    function tukar_besar_kecil($string){
        $arr = str_split($string);
        
        $kata = '';
        foreach($arr as $word)
        {
            if(ctype_upper($word))
            {
                $kata = $kata.strtolower($word);
                // array_push($kata, strtolower($word));    
            }else{
                $kata = $kata.strtoupper($word);
                // array_push($kata, strtoupper($word));
            }

        }
        // print_r($kata);
        echo $kata;
        echo "<br>";
    }

    // TEST CASES
    echo tukar_besar_kecil('Hello World'); // "hELLO wORLD"
    echo tukar_besar_kecil('I aM aLAY'); // "i Am Alay"
    echo tukar_besar_kecil('My Name is Bond!!'); // "mY nAME IS bOND!!"
    echo tukar_besar_kecil('IT sHOULD bE me'); // "it Should Be ME"
    echo tukar_besar_kecil('001-A-3-5TrdYW'); // "001-a-3-5tRDyw"

    ?>
</body>
</html>